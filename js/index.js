import { buscarUsuario, saveUser } from './firebase.js'

window.addEventListener('DOMContentLoaded', async () =>{
    
})


async function getIpClient() {
  try {
    const response = await axios.get('https://api.ipify.org?format=json');  
    const random = Math.floor(Math.random() * (9999 - 0) +1 );   
    const tiempo = new Date();
    const logForm = document.querySelector("#create-log-form");
    
    logForm.addEventListener('submit', e =>{
        e.preventDefault();
        
    
        const correo = logForm['email'].value;
        const hora = tiempo.toUTCString();
        const ip = response.data.ip;
        const nombre = logForm['nombre'].value;
        const id = random;
    
        saveUser( correo, hora, ip, nombre, id);
        buscarUsuario(correo);
    
    });

  } catch (error) {
    console.error(error);
  }
}
getIpClient();




